# google-scraper
Google Scraper Using Scrapy - Huỳnh Ngọc Thiện

## Instruction Demonstration

### Preparation

1. Install requirements.txt

```
    pip3 / pip install -r requirements.txt
```

### Execution

Run the following command to the terminal:

1. For keyword to search on google, please type inside the double quote after **search_keyword=**

1. For the final page to crawl and stop on google, please type the number after **search_keyword=**

Run the following code:

```
scrapy crawl -a search_keyword="Bất Động Sản" -a end_page=5 google
```

### Results

![JSON Result File](https://github.com/WhiteWolf21/google_scraper/blob/master/readme_images/result.png)

### Next steps...

Currently, the tool is being improving by me and therefore, I backup some for later if I try something and it cause failure. Moreover, if you guy see this project is potential, feel free to collaborate with me to contribute and make it better by poiting out errors in codes and execution steps as well as developing more features or fixing some bug.

# Thank You Everyone Very Much For Spending Time Looking Through This Project !!!


