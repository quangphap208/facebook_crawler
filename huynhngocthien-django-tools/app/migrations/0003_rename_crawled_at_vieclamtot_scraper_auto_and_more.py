# Generated by Django 4.0.2 on 2022-03-25 05:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_alter_facebook_comment_scraper_created_at_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='vieclamtot_scraper',
            old_name='crawled_at',
            new_name='auto',
        ),
        migrations.RenameField(
            model_name='vieclamtot_statistic',
            old_name='num_post',
            new_name='assistant',
        ),
        migrations.RemoveField(
            model_name='vieclamtot_scraper',
            name='is_crawled',
        ),
        migrations.RemoveField(
            model_name='vieclamtot_statistic',
            name='search_keyword',
        ),
        migrations.AddField(
            model_name='vieclamtot_scraper',
            name='job_id',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='audit',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='beauty_care',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='builder',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='carpenter',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='chef_bartender',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='customer_care',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='driver',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='electrician',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='food_processor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='guard',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='maid',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='mechanic',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='metalist',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='multi_industry',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='real_estate',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='receptionist',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='restaurant_hotel',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='salesman',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='seller',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='shipper',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='unskilled_labor',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='weaver',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vieclamtot_statistic',
            name='worker',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='facebook_comment_scraper',
            name='created_at',
            field=models.FloatField(default=1648187131.584181),
        ),
        migrations.AlterField(
            model_name='facebook_comment_scraper',
            name='timestamp',
            field=models.FloatField(default=1648187131.584174),
        ),
        migrations.AlterField(
            model_name='facebook_comment_scraper',
            name='updated_at',
            field=models.FloatField(default=1648187131.584186),
        ),
        migrations.AlterField(
            model_name='facebook_post_scraper',
            name='created_at',
            field=models.FloatField(default=1648187131.583232),
        ),
        migrations.AlterField(
            model_name='facebook_post_scraper',
            name='timestamp',
            field=models.FloatField(default=1648187131.583218),
        ),
        migrations.AlterField(
            model_name='facebook_post_scraper',
            name='updated_at',
            field=models.FloatField(default=1648187131.583238),
        ),
        migrations.AlterField(
            model_name='facebook_reaction_scraper',
            name='created_at',
            field=models.FloatField(default=1648187131.584569),
        ),
        migrations.AlterField(
            model_name='facebook_reaction_scraper',
            name='updated_at',
            field=models.FloatField(default=1648187131.584576),
        ),
        migrations.AlterField(
            model_name='facebook_user_profile_scraper',
            name='created_at',
            field=models.FloatField(default=1648187131.585018),
        ),
        migrations.AlterField(
            model_name='facebook_user_profile_scraper',
            name='updated_at',
            field=models.FloatField(default=1648187131.585026),
        ),
        migrations.AlterField(
            model_name='facebook_user_scraper',
            name='created_at',
            field=models.FloatField(default=1648187131.583608),
        ),
        migrations.AlterField(
            model_name='facebook_user_scraper',
            name='updated_at',
            field=models.FloatField(default=1648187131.583615),
        ),
    ]
