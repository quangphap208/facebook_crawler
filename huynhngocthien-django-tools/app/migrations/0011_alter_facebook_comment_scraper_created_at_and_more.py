# Generated by Django 4.0.2 on 2022-05-03 10:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_alter_facebook_comment_scraper_created_at_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='facebook_comment_scraper',
            name='created_at',
            field=models.FloatField(default=1651573184.883199),
        ),
        migrations.AlterField(
            model_name='facebook_comment_scraper',
            name='timestamp',
            field=models.FloatField(default=1651573184.883193),
        ),
        migrations.AlterField(
            model_name='facebook_comment_scraper',
            name='updated_at',
            field=models.FloatField(default=1651573184.883205),
        ),
        migrations.AlterField(
            model_name='facebook_post_scraper',
            name='created_at',
            field=models.FloatField(default=1651573184.882516),
        ),
        migrations.AlterField(
            model_name='facebook_post_scraper',
            name='timestamp',
            field=models.FloatField(default=1651573184.882501),
        ),
        migrations.AlterField(
            model_name='facebook_post_scraper',
            name='updated_at',
            field=models.FloatField(default=1651573184.882521),
        ),
        migrations.AlterField(
            model_name='facebook_reaction_scraper',
            name='created_at',
            field=models.FloatField(default=1651573184.883522),
        ),
        migrations.AlterField(
            model_name='facebook_reaction_scraper',
            name='updated_at',
            field=models.FloatField(default=1651573184.883529),
        ),
        migrations.AlterField(
            model_name='facebook_user_profile_scraper',
            name='created_at',
            field=models.FloatField(default=1651573184.883825),
        ),
        migrations.AlterField(
            model_name='facebook_user_profile_scraper',
            name='updated_at',
            field=models.FloatField(default=1651573184.883831),
        ),
        migrations.AlterField(
            model_name='facebook_user_scraper',
            name='created_at',
            field=models.FloatField(default=1651573184.88277),
        ),
        migrations.AlterField(
            model_name='facebook_user_scraper',
            name='updated_at',
            field=models.FloatField(default=1651573184.882776),
        ),
        migrations.AlterField(
            model_name='vieclamtot_scraper',
            name='post_time',
            field=models.BigIntegerField(null=True),
        ),
    ]
